# pynetburner_gpio

Python library which implements a subset of the the network GPIO "multi-function
pin control" and "machine command mode" for the [Netburner
SBL2e](https://www.netburner.com/products/serial-to-ethernet/sbl2e/). The
protocol is documented in the [SBL2e User
Manual](https://www.netburner.com/download/sbl2e-dual-serial-to-ethernet-users-manual/)
starting on page 18.

## Command summary

From the user manual:

The machine command mode is so named because the data sent and received are in hexadecimal or binary, as opposed to the AT command mode, which is more of a human-readable text syntax format. All non-programmable pins such as power, ground, and /RSTI will read back as zero and programming commands will have no effect on them.

Syntax

* All commands are terminated with the line feed character (0x10 in hexadecimal representation; also represented as ‘\n’ in the C language). The carriage return character (0x13 or ‘\r’) is ignored, so commands may be terminated with ‘\n’ or “\r\n”.
* Command parameters are in hexadecimal format, with the most significant bit (MSB) first. Return values
* Commands to set parameters will return a status value of 0 on success, or 1 for a syntax error.
* Commands that return values will be in the format: “<status>,<result>”, where status is 0 (success) or 1 (syntax error) and the result is one or more hexadecimal values, depending on the command. Hexadecimal values display the most significant bit first.
* Return values are terminated by “\r\n”.

| Command | |
|---------|-|
| ```MGxxxx``` | GPIO enable: Set the pin configuration: primary function (0) or GPIO mode (1). |
| ```MG``` | GPIO enable query: Return the pin configuration values. |
| ```MRxxxx``` | GPIO direction: Set the GPIO pin direction: input (0) or output (1). | 
| ```MR``` | GPIO direction query – Return the GPIO pin direction values. |
| ```MOxxxx``` | GPIO output set – Set individual GPIO output pins: low (0) or high (1). |
| ```MO``` | GPIO output query – Return the programmed state of all GPIO output pins. |
| ```MH``` | Set all GPIO output pins high (1). |
| ```ML``` | Set all GPIO output pins low (0). |
| ```MP``` | Save current settings as the initial power-on state. |
| ```MQ``` | Query all A/D and GPIO values. |
| ```MT``` | Terminate the GPIO command TCP connection. |
| ```M?``` | Return all GPIO input and output pin values: low (0) or high (1). |

## Minimum example

```
import serial
from pydspl_seasense import sealite

with serial.serial_for_url("/dev/ttyUSB0") as fp:

  sealite = sealite.Sealite(address="001")

  print(sealite.read_temperature(fp))
```

# License

[BSD](LICENSE.txt)
