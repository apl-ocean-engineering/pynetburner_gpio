#!/usr/bin/env python3

import argparse
from pynetburner_gpio.netburner import Netburner

def cli_status(args, netburner):
    netburner.dump()

def cli_save(args, netburner):
    netburner.save()

def cli_set_alt_func(args, netburner):
    for pin in args.pins:
        netburner.pin(pin).set_mode(alt_func=True)
    netburner.apply()

def cli_set_input(args, netburner):
    for pin in args.pins:
        netburner.pin(pin).set_direction(input=True)
        netburner.pin(pin).set_mode(alt_func=False)
    netburner.apply()

def cli_set_out_high(args, netburner):
    set_output(args, netburner, True)

def cli_set_out_low(args, netburner):
    set_output(args, netburner, False)

def set_output(args, netburner, val):
    for pin in args.pins:
        netburner.pin(pin).set_value(val)
        netburner.pin(pin).set_direction(input=False)
        netburner.pin(pin).set_mode(alt_func=False)
    netburner.apply()


def main():

    parser = argparse.ArgumentParser(description='Simple command line test program for netburner GPIO interface')

    # Global argparse arguments
    parser.add_argument("--host",
                        default="10.10.10.134",
                        help="Hostname/IP address for Netburner")
    parser.add_argument("--port",
                        default=1000,
                        help="Port for interface")

    subparser = parser.add_subparsers(required=True,dest='command')

    subparser.add_parser('status').set_defaults(func=cli_status)

    subparser.add_parser('save').set_defaults(func=cli_save)


    output_parser = subparser.add_parser('func')
    output_parser.set_defaults( func=cli_set_alt_func )
    output_parser.add_argument("pins", nargs="+", type=int, help="Pin(s) to set to alt function")

    output_parser = subparser.add_parser('input')
    output_parser.set_defaults( func=cli_set_input )
    output_parser.add_argument("pins", nargs="+", type=int, help="Pin(s) to set to input")

    output_parser = subparser.add_parser('out_high')
    output_parser.set_defaults( func=cli_set_out_high )
    output_parser.add_argument("pins", nargs="+", type=int, help="Pin(s) to set to input")

    output_parser = subparser.add_parser('out_low')
    output_parser.set_defaults( func=cli_set_out_low )
    output_parser.add_argument("pins", nargs="+", type=int, help="Pin(s) to set to input")


    args = parser.parse_args()

    netburner = Netburner( host=args.host, port=args.port )

    args.func(args, netburner)

    netburner.dump()


if __name__=="main":
    main()