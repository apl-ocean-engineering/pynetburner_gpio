
from pynetburner_gpio.netburner_socket import NetburnerHexResult

def compare_results( result, ground_truth):
    assert len(result.bits) == len(ground_truth)

    for i in range(len(ground_truth)):
        assert( result.bit(i) ) == ground_truth[i]
        assert( result[i] ) == ground_truth[i]

def test_good_response():
    
    result = NetburnerHexResult.from_bytes( b'0,0x1234' )

    assert type(result) is NetburnerHexResult
    
    assert result.result is 0
    assert result.data == "0x1234"
    assert result.valid

    ## Remember the hex value is MSB, so this array is "backwards"
    ground_truth = [False, False, True, False, 
                    True, True, False, False, 
                    False, True, False, False, 
                    True, False, False, False]

    assert len(result.bits) == 16
    compare_results( result, ground_truth )

    assert result.int() == 0x1234

    ## Set bits

    for i in [4,12,7,12]:
        ground_truth[i] = not ground_truth[i]
        result[i] = not result[i]

    compare_results( result, ground_truth )
