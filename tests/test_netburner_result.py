from pynetburner_gpio.netburner_socket import NetburnerResult

def test_good_response():
    
    result = NetburnerResult.from_bytes( b'0,0x1234' )

    assert type(result) is NetburnerResult
    
    assert result.result is 0
    assert result.data == "0x1234"
    assert result.valid

def test_bad_response():
    result = NetburnerResult.from_bytes( b'1' )

    assert type(result) is NetburnerResult
    assert not result.valid
