
import serial

from .netburner_socket import NetburnerGPIOSocket, NetburnerResultFromArray

ALT_FUNC = ["UART0_TX","UART0_RX",
            "UART0_RTS","UART0_CTS",
            "VCC3V","GND",
            "ADC_IN0","ADC_IN1",
            "ADC_IN2","ADC_IN3",
            "GND","UART1_RX",
            "UART1_TX","UART2_TX",
            "UART2_TX", "/RESET"]

class NetburnerPin:

    def __init__(self,i,mode,direction,value):
        self._i = i

        self.update(mode,direction,value)

    def update(self, mode,direction,value):
        self._mode = mode
        self._direction = direction
        self._value = value

    @property
    def number(self):
        return self._i

    @property
    def mode(self):
        return self._mode

    def mode_str(self):
        if self._mode:
            if self._direction:
                return "GPIO Out"
            else:
                return "GPIO In"
        else:
            return ALT_FUNC[self.number-1]

    def set_mode(self, alt_func):
        self._mode = 0 if alt_func else 1

    @property
    def value(self):
        return self._value

    def set_value(self, val):
        self._value = val

    def value_str(self):
        if self._mode:
            return str(self._value)
        else:
            return ""

    @property
    def direction(self):
        return self._direction

    def set_direction(self, input):
        self._direction = 0 if input else 1

class Netburner:
    """
    Base class which provides generic I/O and commands common across all
    DSP&L devices.
    """

    def __init__(self, host="", port=1000, fp=None):
        self._socket = NetburnerGPIOSocket( host, port, fp )

        mode = self._socket.query_mode()
        direction = self._socket.query_direction()
        value = self._socket.query_all_gpio()

        if not mode.valid or not direction.valid or not value.valid:
            assert "Bad return from Netburner"

        self._pins = [NetburnerPin(*i) for i in zip(range(1,17),mode,direction,value)]

    def pin(self,i):
        return self._pins[i-1]

    @property
    def socket(self):
        return self._socket

    def update(self):
        mode = self._socket.query_mode()
        direction = self._socket.query_direction()
        value = self._socket.query_all_gpio()

        for (pin,*vals) in zip( self._pins,mode,direction,value):
            pin.update(*vals)

    def apply(self):

        values = NetburnerResultFromArray([(pin.value) for pin in self._pins])
        result = self._socket.set_output(values)
        if not result.valid:
            assert "Invalid result"

        directions = NetburnerResultFromArray([(pin.direction) for pin in self._pins])
        result = self._socket.set_direction(directions)
        if not result.valid:
            assert "Invalid result"

        modes = NetburnerResultFromArray([(pin.mode) for pin in self._pins])
        result = self._socket.set_mode(modes)
        if not result.valid:
            assert "Invalid result"

    def save_as_default(self):
        self._socket.save_as_default()

    def dump(self):
        for pin in self._pins:
            print("%2d   %9s   %9s" % (pin.number,pin.mode_str(),pin.value_str()))
