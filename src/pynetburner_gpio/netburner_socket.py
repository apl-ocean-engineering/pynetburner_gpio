
import serial

class NetburnerResult:

    def __init__(self, errcode, data):
        """
        Valid response will have two forms.
        A command will return "0" (success) or "1" (error)
        A query will return "0,<data>" or "1"
        """
        
        self._data = data
        self._result = errcode

    @classmethod
    def from_bytes( cls, result_bytes ):
        results = result_bytes.decode('ascii').split(',')

        if len(results) == 1:
            return cls( int(results[0]), b'' )
        else:
            return cls( int(results[0]), results[1].strip() )

    @property
    def result(self):
        return self._result

    @property
    def valid(self):
        return self._result == 0

    @property
    def data(self):
        return self._data


class NetburnerHexResult(NetburnerResult):
    """
    Many of the Netburner responses contain a hex representation of a 16-bit bitfield
    (1 bit per pin on the Netburner).  
    """

    def __init__(self, errcode, data ):
        super().__init__( errcode, data )

        if self.valid:
            # Use zfill to pad the string with zeroes
            bits_string = bin(int( self.data, 16 ))[2:]
            padded_len = 8 * (int(len(bits_string)/8)+1)

            ### Hex value is MSB, but store the _bits array_ with Python ordering
            bits_string = bits_string.zfill(padded_len)[::-1]
            self._bits = [( int(bit) == 1 ) for bit in bits_string]

    @property
    def bits(self):
        return self._bits    

    def bit(self, i):
        return self._bits[i]

    def int(self):
        """
        Returns current state as an integer
        """
        ## this feels janky
        bits_string = "".join(["1" if bit else "0" for bit in self.bits][::-1])

        return int(bits_string,2)


    def __getitem__(self, i):
        return self._bits[i]

    def __setitem__(self, i, v):
         self._bits[i] = v
         return self._bits[i]

class NetburnerResultFromArray(NetburnerHexResult):

    def __init__(self, array):
        self._result = 0  ## Always valid

        if( len(array) != 16 ):
            assert "Array is wrong length"

        self._bits = array


class NetburnerGPIOSocket:
    """
    Base class which provides generic I/O and commands common across all
    DSP&L devices.
    """

    def __init__(self, host, port=1000, fp=None):
        self._address = host
        self._port = port

        if not fp:
            self.fp = serial.serial_for_url("socket://%s:%d/" % (self._address, self._port))
            self.fp.timeout = 1
        else:
            self.fp = fp

    @property
    def address(self):
        return self._address

    @property
    def port(self):
        return self._port

    def do_command(self, command, response_cls=NetburnerResult):
        """ 
        Generic accessor function.

        While all communication with the Netburner occur in binary,
        this function expects and returns strings 
        """

        command = command + "\n"

        self.fp.write( command.encode('ascii') )
        result = self.fp.read_until()

        return response_cls.from_bytes( result )

    ## Wrappers around specific commands
    def query_all(self):
        """
        Sends the "MQ" command which "queries all AD and GPIO values"

        This appears to be broken on the current version of the firmware,
        It stops after the first 16-bit return, then returns the remainder
        and an error message after another keystroke
        """

        return self.do_command("MQ")

    def query_all_gpio(self):
        """
        Sends the "M?" command which "queries all GPIO values"
        """

        return self.do_command("M?", response_cls=NetburnerHexResult)

    def query_direction(self):
        """
        Sends the "MR" command which "queries the direction of all pins"
        with 0 == input, 1 == output
        """
        return self.do_command("MR", response_cls=NetburnerHexResult)

    def set_direction(self, result):
        """
        Send the MRxxxx command to _set_ pin functions

        Assumes you will be manipulating a small number of bits, requires as 
        NetburnerHexResult as an input
        """

        if not isinstance(result, NetburnerHexResult):
            raise TypeError

        return self.do_command("MR%04x" % result.int() )

    def query_mode(self):
        """
        Sends the "MG" command which "queries the function of all pins"
        with 0 == primary function, 1 == GPIO
        """
        return self.do_command("MG", response_cls=NetburnerHexResult)

    def set_mode(self, result):
        """
        Send the MGxxxx command to _set_ pin functions

        Assumes you will be manipulating a small number of bits, requires a 
        NetburnerHexResult as an input
        """

        if not isinstance(result, NetburnerHexResult):
            raise TypeError

        return self.do_command("MG%04x" % result.int() )

    def query_output(self):
        """
        Sends the "MO" command which "queries the programmed state of all GPIO output pins"
        with 0 == low, 1 == high
        """
        return self.do_command("MO", response_cls=NetburnerHexResult)

    def set_output(self, result):
        """
        Send the MOxxxxx which sets individual GPIO output pins

        Assumes you will be manipulating a small number of bits, requires as 
        NetburnerHexResult as an input
        """

        if not isinstance(result, NetburnerHexResult):
            raise TypeError

        return self.do_command("MO%04x" % result.int() )

    def save_as_default( self):
        """
        Sends \"MP\" which saves the current configuration as the power-on
        default.
        """
        return self.do_command("MP")

    def set_all_high(self):
        return self.do_command("MH")

    def set_all_low(self):
        return self.do_command("ML")

    def version(self):
        """
        Returns the response from the "V" command -- "version"
        """

        return self.do_command("V")

